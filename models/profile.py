from db import db

class ProfileModel(db.Model):

    __tablename__ = "profiles"
    id = db.Column(db.String(20), primary_key=True, unique=True)
    first_name = db.Column(db.String(20), unique=False, nullable=False)
    last_name = db.Column(db.String(20), unique=False)
    age = db.Column(db.Integer, nullable=True)

    def __repr__(self):
        return f"Name: {self.first_name}, age: {self.age}."

    @classmethod
    def find_my_name(cls, first_name, last_name):
        return cls.query.filter_by(first_name=first_name, last_name = last_name).first()

    def json(self):
        return {'first_name': self.first_name, 'last_name': self.last_name, 'age': self.age}

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
